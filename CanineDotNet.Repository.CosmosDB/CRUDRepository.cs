﻿namespace CanineDotNet.Repository.CosmosDB
{
    using CanineDotNet.Common.Exceptions;
    using CanineDotNet.Common.Interfaces;
    using CanineDotNet.Repository.Interfaces;
    using CanineDotNet.Repository.Interfaces.Repositories;
    using Microsoft.Azure.Cosmos;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public class CRUDRepository<TEntity, TKey> : ICRUDRepository<TEntity, TKey> where TEntity : ILogableEntity<TKey>
    {
        protected Container _container;
        protected readonly IDateTime _dateTime;

        public CRUDRepository(IDateTime dateTime, CosmosClient dbClient, string databaseName, string containerName)
        {
            this._container = dbClient.GetContainer(databaseName, containerName);
            this._dateTime = dateTime;
        }

        public async Task<IEnumerable<TEntity>> GetAllAsync()
        {
            return await this._container.GetItemQueryIterator<TEntity>().ReadNextAsync();
        }

        public async Task<TEntity> GetAsync(TKey id)
        {
            try
            {
                ItemResponse<TEntity> response = await this._container.ReadItemAsync<TEntity>(id.ToString(), new PartitionKey(id.ToString()));
                return response.Resource;
            }
            catch (CosmosException ex) when (ex.StatusCode == System.Net.HttpStatusCode.NotFound)
            {
                throw new ElementDoesNotExistException("Item does not exist", id.ToString());
            }
        }

        public Task CreateAsync(TEntity item)
        {
            item.CreatedDate = _dateTime.UtcNow;
            return this._container.CreateItemAsync(item, new PartitionKey(item.Id.ToString()));
        }

        public async Task UpdateAsync(TKey id, TEntity item)
        {
            var existingItem = await GetAsync(item.Id);
            item.CreatedDate = existingItem.CreatedDate;
            item.LastModifiedDate = _dateTime.UtcNow;
            await this._container.ReplaceItemAsync(item, id.ToString(), new PartitionKey(id.ToString()));
        }

        public Task DeleteAsync(TKey id)
        {
            return this._container.DeleteItemAsync<TEntity>(id.ToString(), new PartitionKey(id.ToString()));
        }
    }
}
