﻿namespace CanineDotNet.Repository.CosmosDB
{
    using CanineDotNet.Repository.Interfaces;
    using CanineDotNet.Repository.Interfaces.Repositories;
    using Microsoft.Azure.Cosmos;

    public class Repository<TEntity, TKey> : IRepository<TEntity, TKey> where TEntity : IEntity<TKey>
    {
        private Container _container;

        public Repository(CosmosClient dbClient, string databaseName, string containerName)
        {
            this._container = dbClient.GetContainer(databaseName, containerName);
        }
    }
}
