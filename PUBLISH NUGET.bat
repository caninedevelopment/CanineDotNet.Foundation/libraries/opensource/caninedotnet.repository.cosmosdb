set /p ver="Version: "

dotnet build -o Artifacts/%ver% -p:Version=%ver%
nuget push .\Artifacts\%ver%\*%ver%.nupkg -Source CanineDotNet -NonInteractive